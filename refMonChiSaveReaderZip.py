from os import walk

import zipfile
from matplotlib import pyplot as plt
import pandas as pd
import numpy as np
import logging
from numpy import double
import time
import datetime as dt
import matplotlib.dates as md
import argparse
import warnings

LOGGER = logging.getLogger("__name__")

def _readTIME(archive):
    data = archive.read('TIME.dat')
    if data is None:
        raise Exception('There is no TIME.dat')
    
    dt = np.dtype(double)
    dt = dt.newbyteorder('>')
    arr = np.frombuffer(data, dtype=dt)
    
    return arr.byteswap().newbyteorder()
    
def _getFirstTimeIndex(last,timearr):
    idx = 0
    for t in timearr:
        if t > last:
            break;
        idx +=1
    print('First Index to read : ',idx)
    return idx 

def _readDirectory(mydirs):
    ''' Reads single directory
        mydirs: string with comma separated paths of directories
    ''' 
    
    ndirs = len(mydirs);
    print('Paths ', mydirs,' ndirs = ', ndirs)
    
    lasttime = 0
    
    df = None
    dirno = 0
    
    for dirpath in mydirs.split(','):
    
        filelist = listOfSortedFiles(dirpath)
        dirno = dirno + 1
        
        if filelist is None:
            warnings.warn("Found no files in directory %s, or the directory does not exit."%dirpath)
            continue 
        
        for fn in filelist:
            ffn = dirpath+'/'+fn
            print('')
            print(ffn)
            archive = zipfile.ZipFile(ffn, 'r')
            
            try:
                thistimearr = _readTIME(archive)
            except Exception as ex:
                LOGGER.error('Error %s while reading file <%s>', repr(ex),ffn)
                continue
            
            startidx = _getFirstTimeIndex(lasttime, thistimearr)
                
            
            df1f = pd.DataFrame()
            
            for dfn in archive.filelist:
                #print("Reading data files ", dfn)
                data = archive.read(dfn)
                #datalen = len(data)
                #print('There ',datalen,' bytes')
                dt = np.dtype(double)
                dt = dt.newbyteorder('>')
                arr = np.frombuffer(data, dtype=dt)
    
                #return 
                
                #data_arr = np.append(data_arr, arr , axis=0)
                #print(print)
                narr = arr.byteswap().newbyteorder()
                
                df1f[dfn.filename.replace('.dat','')] = narr[startidx:]
                
            if df is None:
                df = df1f
            else:        
                df = df.append(df1f, ignore_index=True)    
              
            
            archive.close()
            if startidx < len(thistimearr):
                lasttime = thistimearr[-1] # get the last element
            else:
                warnings.warn("All data in file %s are older than expected."%fn)
                print("Either data are inconsistent or this is a bug")
                print("Last timestamp in this file ",time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(thistimearr[-1]/1000.)))
             
            print('dirno ',dirno, ': Data up to time ', time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(lasttime/1000.)))
            print('dirno ',dirno, ': New data size ', df.as_matrix().shape)
    
    df['TIME'] = df.loc[:,'TIME'] / 1e3;              
    return df         

#___________________________________________________
        
def _plot(df,dataname):
    
    values = df[dataname]
    dates=[dt.datetime.fromtimestamp(ts) for ts in df['TIME']]
    plt.subplots_adjust(bottom=0.2)
    plt.xticks( rotation=25 )
    ax=plt.gca()
    xfmt = md.DateFormatter('%Y-%m-%d %H:%M:%S')
    
    ax.xaxis.set_major_formatter(xfmt)
    plt.plot(dates,values)
    
    #plt.plot(dates,df.iloc[:,92])
    #plt.plot(dates,df.iloc[:,104])
    #plt.plot(dates,df.iloc[:,105])
    #plt.plot(dates,df.iloc[:,106])
    
    plt.show()

    for tj in df['TIME']:
        print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(tj/1000.)))
        break

    # alternative 
    #df.set_index('TIME', drop=False, inplace=True)
    #df.loc[:,'Intesity/EI.BCT10-ST.chi2'].plot()
    #plt.show()
#___________________________________________________
    
def  listOfSortedFiles(directorypath):    
    ''' Returns list of sorted files from a data directory '''
    filenames = None
    
    try:
        for (dirpath, dirnames, filenames) in walk(directorypath):
            break;
    except Exception as ex:
        print('Exception %s while processing directory %s '%(type(ex),directorypath))  
        print(ex.args)    
        print(ex)
        return
    
    if filenames is None:
        print('Exception while processing directory %s '%(directorypath))
        return
                      
#     print("dirpath ",dirpath)
#     print("dirnames ",dirnames)
#     print("filenames ",filenames)
    
    filenames = sorted(filenames)
    #filenames.sort(key=lambda s: os.path.getmtime(os.path.join(dirpath, s)))
    
    fnDict = {}
    for fn in filenames:
        #print("fn ", fn)
        uidx = fn.rfind('_')
        cidx = fn.find(':')
        #print(cidx - uidx)
        if (cidx - uidx) > 2:
            #print("cupying ")
            cfn = fn;
        else:
            cfn = fn[0:uidx+1]
            cfn += '0'
            cfn += fn[uidx+1:]
            #print ("new file is ",cfn)
        
        fnDict[cfn] = fn
        
    fnDictSorted = sorted(fnDict)
    listOfSortedFiles = []
    #print(' vvvvvvvvvv')
    for fn in fnDictSorted:
        #print(fn,' --> ',fnDict[fn])
        listOfSortedFiles.append(fnDict[fn])
    
    return listOfSortedFiles 

def _printColumnNames(df):
    colnames = list(df.columns.values)
    i=1
    for cn in colnames:
        print('Column ',i,' ', cn)
        i += 1
#___________________________________________________

def _convert_java_millis(java_time_millis):
    """Provided a java timestamp convert it into python date time object"""
    ds = dt.datetime.fromtimestamp(
        int(str(java_time_millis)[:10])) if java_time_millis else None
    ds = ds.replace(hour=ds.hour,minute=ds.minute,second=ds.second,microsecond=int(str(java_time_millis)[10:]) * 1000)
    return ds

#___________________________________________________

def _parse_args():
    parser = argparse.ArgumentParser(description='Does correlation analysis of Reference Monitor data.')
    
    parser.add_argument("-d", "--dirs",
                    help="Directories with data, separated by comma",
                    metavar="DIRS", default=".", dest="dirs")
    args = parser.parse_args()
    
    return args

#___________________________________________________


if __name__ == "__main__":
    
    opts = _parse_args()
    
    df = _readDirectory(opts.dirs)
    
    _printColumnNames(df)
    dataname = 'Intesity/EI.BCT10-ST';
    _plot(df,dataname)
    


#######################################################    
    
#                 j=0
#             for i in range(0,datalen,8):
#                 valbytes =  data[i:i+8]
#                 val = struct.unpack('>d', valbytes)
#                 print(val,data_arr[j])
#                 j += 1

#             while True:
#                 valbytes =  data.read(4)
#                 if not valbytes:
#                     break;
#                 val = struct.unpack('f', valbytes)
#                 print(val)
                
