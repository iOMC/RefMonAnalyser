"""
.. module: correlation_analysis

Created on 03/03/17
:author: Lukas Malina

Runs the correlation analysis of 2D numpy array
and animates matrices (all_devices x all_devices) of color-coded R^2_{non-zero}
in a sliding time window as described in:
    L.~Malina et al., Drive beam stabilisation in the CLIC Test Facility 3,
    Nucl. Instrum. Methods Phys. Res. A vol. 894, p. 25-32 (2018).

"""
from __future__ import print_function
import os
import sys
import time
import datetime

from scipy.stats import t
import matplotlib.animation as animation
from matplotlib import rc
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import refMonChiSaveReaderZip
import argparse

DEBUG = sys.flags.debug

# Defaults for calling get_running_determination_matrix
# see comments in the function
defWLEN     = 2000
defSAMPLING = 200

DIRS       = "."       #@IgnorePep8

def _parse_args():
    parser = argparse.ArgumentParser(description='Does correlation analysis of Reference Monitor data.')
    
    parser.add_argument("-d", "--dirs",
                    help="Directories with data, separated by comma",
                    metavar="DIRS", default=DIRS, dest="dirs")
    args = parser.parse_args()
    
    return args
    

def correlation_analysis(data=np.random.rand(50, 300), create_anim=True):
    """
    Runs the correlation analysis of the data

    Animates a matrices (all_devices x all_devices) of color-coded R^2_{non-zero}
    in a sliding time window

    :param data: 2-dimensional numpy array, where first index stands for device No., second index
    is related to position in a time-serie
    the last row (device) is assumed to be a timestamp
    :param create_anim: Boolean stating whether an animation is created
    """
    # setup of a figure, title, plot, labels and color bar
    set_plot_params(18, 14)
    devs = data.shape[0] 
    print('There is %d devices.'%devs)
    f, ax = plt.subplots(1, figsize=(10, 10))
    mat = ax.matshow(np.random.rand(devs, devs))
    tit = ax.set_title(
        'Running lower limit on coefficient of determination \n among CTF3 Devices, time: 0')
    ax.tick_params(axis='both', which='both', bottom='on', top='on', labelbottom='on', right='on',
                   left='on', labelleft='on', labeltop='off')
    ax.set_xlabel('signal No. [-]')
    ax.set_ylabel('signal No. [-]')
    cbar = f.colorbar(mat, ticks=[0, 0.2, 0.4, 0.6, 0.8, 1.0], fraction=0.046, pad=0.04)
    cbar.set_label('$R^2_{non-zero}$ [-]')
    f.tight_layout()

    def run(d):  # update the animation frame data and frame title (title of the plot)
        de, t = d
        mat.set_data(de)
        tit.set_text('Running lower limit on coefficient of determination \n, time: {0}'.format(
            datetime.datetime.fromtimestamp(t)))
        print(' >> %s '%(datetime.datetime.fromtimestamp(t)))
        return mat, tit

    wlen = defWLEN
    sampling = defSAMPLING
    if create_anim:
        animate = get_running_determination_matrix(data,wlen,sampling)
        print("Will produce %d movie frames "%len(animate))
        newmatrix, listof = get_3D_determination_matrix(animate, data.shape[0], 0.0)
        de, t = listof[2]
        mat.set_data(de)
        ani = animation.FuncAnimation(f, run, listof, blit=True, interval=100, repeat=False)
        print('Saving GIF ... ')
        ani.save("test.gif", writer='imagemagick')
        print('Saving GIF ... finished ')


def get_running_determination_matrix(data, wlen=200, sampling=10):
    """
        Runs the correlation analysis (with hardcoded significance level),
         removes intervals with too much missing data (TO BE SET)
        :param data: 2-dimensional numpy array, where first index stands for device No., second index
        is related to position in a time-serie
        the last row (device) is assumed to be a timestamp
        :param wlen: length of a sliding time window
        :param sampling: number of values in a time-serie between starts of two consecutive time windows
        :return: a chronological list of valid determination matrices (all_devices x all_devices)
            a list is needed for matplotlib.animation.FuncAnimation
        """
    # 0.8413,0.9772,0.9987 ## 1,2,3 sigmas

    result = []
    error = t.ppf(0.9987, wlen - 1) / np.sqrt(wlen - 2)
    for i in range(0, data.shape[1] - wlen, sampling):
        b = np.arctanh(
            np.abs(np.corrcoef(data[:, i:i + wlen])) - np.eye(data.shape[0]) * 1e-15) - error  #
        res = (np.tanh(np.where(b > 0.0, b * b, np.zeros_like(b))), data[-1][i])
        # the last row is time and this checks if the time span of a single interval is not too long
        
        step = (data[-1][i + wlen] - data[-1][i])
        if step < 1e6:
            result.append(res)
        else:
            print('Too large step ', step)
    return result


def get_3D_determination_matrix(listof2D, dim, R2lim):
    # minimal limit on maximal R^2_non-zero for a given pair of signals,
    # if 0.0 then it just makes the plot nicer
    Rmatrix3D = np.empty([dim, dim, len(listof2D)])
    for a in range(0, len(listof2D)):
        m, t = listof2D[a]
        newm = m - np.eye(dim)  # np.tril(m,-1)
        Rmatrix3D[:, :, a] = newm

    b = np.amax(np.amax(Rmatrix3D, axis=2), axis=1)
    for a in range(0, dim):
        Rmatrix3D[a, :, :] = np.where(b[a] > R2lim, Rmatrix3D[a, :, :],
                                      np.zeros_like(Rmatrix3D[a, :, :]))
    b = np.amax(np.amax(Rmatrix3D, axis=2), axis=0)
    for a in range(0, dim):
        Rmatrix3D[:, a, :] = np.where(b[a] > R2lim, Rmatrix3D[:, a, :],
                                      np.zeros_like(Rmatrix3D[:, a, :]))
    c = np.amax(Rmatrix3D, axis=2)
    for a in range(0, dim):
        for b in range(0, dim):  # a
            Rmatrix3D[a, b, :] = np.where(c[a, b] > R2lim, Rmatrix3D[a, b, :],
                                          np.zeros_like(Rmatrix3D[a, b, :]))
    for a in range(0, len(listof2D)):
        Rmatrix3D[:, :, a] = Rmatrix3D[:, :, a] + np.eye(dim)
        m, t = listof2D[a]
        m = Rmatrix3D[:, :, a]
        listof2D[a] = (m, t)
    c = np.amax(Rmatrix3D, axis=2)
    return c, listof2D


def set_plot_params(labelfontsize, legendfontsize):
    rc('font', **{'family': 'sans-serif', 'serif': ['Computer Modern']})
    params = {'backend': 'pdf',
              'axes.labelsize': labelfontsize,
              'font.size': labelfontsize,
              'axes.titlesize': labelfontsize,
              'legend.fontsize': legendfontsize,
              'xtick.labelsize': labelfontsize,
              'ytick.labelsize': labelfontsize,
              'text.usetex': False,
              'axes.unicode_minus': True,
              'xtick.major.pad': 10,
              'ytick.major.pad': 10,
              'xtick.minor.pad': 10,
              'ytick.minor.pad': 10,
              }
    plt.matplotlib.rcParams.update(params)

def creatFilterMask(df):
    
    print('df.shape[0] ',df.shape[1])
    mask = np.zeros(df.shape[1], dtype=np.bool)
    
    dnames = list(df.columns.values)
    
    
    tidx = dnames.index('TIME')
    print('tidx = ', tidx) 
    
    mask[tidx]=True # select time
    
    for i in range(len(dnames)):
        if not dnames[i].endswith('.chi2'):
            mask[i] = True
        
    
    #mask[92]=True
    #mask[104:107]=True
    #mask[63:135]=True
   
    return mask

def run_correlation_analysis():
    options = _parse_args()
    
    df = refMonChiSaveReaderZip._readDirectory(options.dirs)
    
    dat = df.as_matrix().T
    
    mask = creatFilterMask(df)
    
    dat = dat[mask,:]
    
    correlation_analysis(dat)
    

if __name__ == "__main__":
    
    
    run_correlation_analysis()
    
